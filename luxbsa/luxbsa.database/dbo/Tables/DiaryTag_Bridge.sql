﻿CREATE TABLE [dbo].[DiaryTag_Bridge] (
    [DiaryId] INT NOT NULL,
    [TagId]   INT NOT NULL,
    PRIMARY KEY CLUSTERED ([DiaryId] ASC, [TagId] ASC),
    CONSTRAINT [FK_DiaryTag_Bridge_ToDiary] FOREIGN KEY ([DiaryId]) REFERENCES [dbo].[Diary] ([Id]),
    CONSTRAINT [FK_DiaryTag_Bridge_ToUser] FOREIGN KEY ([TagId]) REFERENCES [dbo].[Tag] ([Id])
);


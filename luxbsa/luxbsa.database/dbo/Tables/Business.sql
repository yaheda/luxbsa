﻿CREATE TABLE [dbo].[Business] (
    [Id]                 INT           IDENTITY (1, 1) NOT NULL,
    [Name]               VARCHAR (250) NULL,
    [RegistrationNumber] VARCHAR (50)  NULL,
    [WebsiteLink]        VARCHAR (250) NULL,
    [Description]        VARCHAR (300) NULL,
    [Address]            VARCHAR (50)  NULL,
    [VideoLink]          VARCHAR (250) NULL,
    [SectorId]           INT           NOT NULL,
    [Country]            VARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Business_ToSector] FOREIGN KEY ([SectorId]) REFERENCES [dbo].[Sector] ([Id])
);


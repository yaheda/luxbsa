﻿CREATE TABLE [dbo].[UserBusiness_Bridge] (
    [UserId]     INT NOT NULL,
    [BusinessId] INT NOT NULL,
    CONSTRAINT [PK_UserBusiness_Bridge] PRIMARY KEY CLUSTERED ([UserId] ASC, [BusinessId] ASC),
    CONSTRAINT [FK_UserBusiness_Bridge_ToBusiness] FOREIGN KEY ([BusinessId]) REFERENCES [dbo].[Business] ([Id]),
    CONSTRAINT [FK_UserBusiness_Bridge_ToUser] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);


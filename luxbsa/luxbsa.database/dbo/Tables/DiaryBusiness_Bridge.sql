﻿CREATE TABLE [dbo].[DiaryBusiness_Bridge] (
    [DiaryId]    INT NOT NULL,
    [BusinessId] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([BusinessId] ASC, [DiaryId] ASC),
    CONSTRAINT [FK_DiaryBusiness_Bridge_ToBusiness] FOREIGN KEY ([BusinessId]) REFERENCES [dbo].[Business] ([Id]),
    CONSTRAINT [FK_DiaryBusiness_Bridge_ToDiary] FOREIGN KEY ([DiaryId]) REFERENCES [dbo].[Diary] ([Id])
);


﻿CREATE TABLE [dbo].[User] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [Email]           VARCHAR (100) NOT NULL UNIQUE,
    [Password]        VARCHAR (250) NOT NULL,
    [Firstname]       VARCHAR (50)  NOT NULL,
    [Surname]         VARCHAR (50)  NULL,
    [Gender]          INT           NULL,
    [DateOfBirth]     DATE          NULL,
    [AboutMe]         VARCHAR (250) NULL,
    [TwitterAuthId]   INT           NULL,
    [Active]          INT           NOT NULL,
    [ActivationToken] VARCHAR(250) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_User_ToTwitter] FOREIGN KEY ([TwitterAuthId]) REFERENCES [dbo].[TwitterAuth] ([Id])
);


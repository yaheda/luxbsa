﻿CREATE TABLE [dbo].[TwitterAuth] (
    [Id]                INT           IDENTITY (1, 1) NOT NULL,
    [AccessToken]       VARBINARY(250) NOT NULL,
    [AccessTokenSecret] VARBINARY(250) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


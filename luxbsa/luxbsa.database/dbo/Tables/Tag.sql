﻿CREATE TABLE [dbo].[Tag] (
    [Id]   INT          IDENTITY (1, 1) NOT NULL,
    [Text] VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


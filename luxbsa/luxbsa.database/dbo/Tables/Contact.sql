﻿CREATE TABLE [dbo].[Contact]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] VARCHAR(100) NULL, 
    [Email] VARCHAR(100) NULL, 
    [Subject] VARCHAR(250) NULL, 
    [Message] VARCHAR(MAX) NULL, 
    [Reason] INT NOT NULL
)

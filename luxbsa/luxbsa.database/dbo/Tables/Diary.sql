﻿CREATE TABLE [dbo].[Diary] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [Title]       VARCHAR (70)  NOT NULL,
    [Content]     VARCHAR (MAX) NOT NULL,
    [UserID]      INT           NOT NULL,
    [DateCreated] DATETIME2 (7) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Diary_ToUser] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([Id])
);


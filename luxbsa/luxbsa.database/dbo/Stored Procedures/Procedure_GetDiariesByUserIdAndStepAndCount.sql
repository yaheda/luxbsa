﻿CREATE PROCEDURE [dbo].[Procedure_GetDiariesByUserIdAndStepAndCount]
	@userId int,
	@step int,
	@count int
AS
	SELECT * FROM (
		SELECT ROW_NUMBER() OVER (ORDER BY Id DESC) AS rownumber,*
		FROM Diary
		WHERE Diary.UserID = @userId
	) AS A
	WHERE rownumber > (@step * @count) AND rownumber <= @count + (@step * @count)
RETURN 0
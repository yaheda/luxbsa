﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace luxbsa.DAL
{
    public static class TwitterAuthDAL
    {
        public static int RemoveById(luxbsaEntities db, int id)
        {
            var twitterAuth = db.TwitterAuths.Where(e => e.Id == id).Single();
            db.TwitterAuths.Remove(twitterAuth);
            return db.SaveChanges();
        }
    }
}
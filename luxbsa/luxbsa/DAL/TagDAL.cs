﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace luxbsa.DAL
{
    public static class TagDAL
    {
        public static List<Tag> SearchTags(luxbsaEntities db, string term)
        {
            return db.Tags.Where(e => e.Text.ToLower().StartsWith(term.ToLower())).ToList();
        }

        public static int AddTag(luxbsaEntities db, Tag tag)
        {
            var tagEntity = db.Tags.Add(tag);
            tag = tagEntity;
            return db.SaveChanges();
        }

        public static bool TagExistsByText(luxbsaEntities db, string text)
        {
            return db.Tags.Any(e => e.Text.ToUpper() == text.ToUpper());
        }

        public static Tag GetTagByText(luxbsaEntities db, string text)
        {
            return db.Tags.Where(e => e.Text.ToUpper() == text.ToUpper()).SingleOrDefault();
        }
    }
}
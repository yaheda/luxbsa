﻿using luxbsa.Helpers;
using luxbsa.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace luxbsa.DAL
{
    public static class UserDAL
    {
        public static int AddUser(luxbsaEntities db, User user)
        {
            user.Password = SecurityHelper.CreateHashString(user.Password);
            db.Users.Add(user);
            var result = db.SaveChanges();
            luxbsa.Hubs.Notification.Instance.UpdateStatistics();
            return result;

        }

        public static int RemoveUser(luxbsaEntities db, int userId)
        {
            var user = db.Users.Where(e => e.Id == userId).Single();
            db.Users.Remove(user);
            luxbsa.Hubs.Notification.Instance.UpdateStatistics();
            var result = db.SaveChanges();
            return result;
        }

        public static int RemoveByActivationToken(luxbsaEntities db, string activationToken)
        {           
            var user = db.Users.Where(e => e.ActivationToken == activationToken).Single();
            var twitterAuthId = user.TwitterAuthId;
            db.Users.Remove(user);

            if (twitterAuthId != null)
                TwitterAuthDAL.RemoveById(db, (int)twitterAuthId);

            luxbsa.Hubs.Notification.Instance.UpdateStatistics();
            var result = db.SaveChanges();
            return result;
        }

        public static User GetByEmailAndPassword(String email, String password)
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                User user = db.Users.Where(e => e.Email == email).SingleOrDefault();

                if (user == null)
                    return null;

                if (SecurityHelper.ValidatePassword(password, user.Password))
                    return user;

                return null;
            }
        }

        public static List<User> GetUsersByEmail(luxbsaEntities db, string email)
        {
            return db.Users.Where(e => e.Email == email).ToList();
        }

        public static User GetById(luxbsaEntities db, int id)
        {
            return db.Users.Where(e => e.Id == id).SingleOrDefault();
        }

        public static User GetByActivationToken(luxbsaEntities db, string token)
        {
            return db.Users.Where(e => e.ActivationToken == token).SingleOrDefault();
        }

        public static List<User> SearchUser(String term)
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                return db.Users.Where(e => e.Firstname.ToLower().StartsWith(term.ToLower()) || 
                    e.Surname.ToLower().StartsWith(term.ToLower())).ToList();
            }
        }

        public static int AddTwitterAuth(luxbsaEntities db, int userId, string accessToken, string accessTokenSecret)
        {
            User user = db.Users.Where(e => e.Id == userId).SingleOrDefault();

            var key = ConfigurationManager.AppSettings[Constants.Instance.KEY_SecurityKey];
            var iv = ConfigurationManager.AppSettings[Constants.Instance.KEY_SecurityIV];

            byte[] encryptedAccessToken = SecurityHelper.Encrypt(accessToken, key, iv);
            byte[] encryptedAccessTokenSecret = SecurityHelper.Encrypt(accessTokenSecret, key, iv);

            TwitterAuth twitterAuth = new TwitterAuth();
            twitterAuth.AccessToken = encryptedAccessToken;
            twitterAuth.AccessTokenSecret = encryptedAccessTokenSecret;

            user.TwitterAuth = twitterAuth;

            var result = db.SaveChanges();
            return result;
        }

        public static TwitterAuth GetTwitterAuth(luxbsaEntities db, int userId)
        {
            return db.Users.Where(e => e.Id == userId).SingleOrDefault().TwitterAuth;
        }

        public static int ActivateUserByActivationToken(luxbsaEntities db, string token)
        {
            User user = db.Users.Where(e => e.ActivationToken == token).Single();
            user.Active = (int)UserModel.ActivationStatus.Active;
            return db.SaveChanges();
        }

        public static int DeactivateUserByActivationToken(luxbsaEntities db, string token)
        {
            User user = db.Users.Where(e => e.ActivationToken == token).Single();
            user.Active = (int)UserModel.ActivationStatus.InActive;
            return db.SaveChanges();
        }

        public static int GetTotal(luxbsaEntities db)
        {
            return db.Users.Count();
        }

        public static int AddTwitterUser(luxbsaEntities db, string email, string name, string activationToken, string accessToken, string accessTokenSecret)
        {
            var key = ConfigurationManager.AppSettings[Constants.Instance.KEY_SecurityKey];
            var iv = ConfigurationManager.AppSettings[Constants.Instance.KEY_SecurityIV];
            var guidPassword = Guid.NewGuid().ToString();

            User twitterUser = new User()
            {
                Firstname = name,
                Email = email,
                Password = guidPassword,
                Active = (int)UserModel.ActivationStatus.InActive,
                ActivationToken = activationToken,
                TwitterAuth = new TwitterAuth() 
                {
                    AccessToken = SecurityHelper.Encrypt(accessToken, key, iv),
                    AccessTokenSecret = SecurityHelper.Encrypt(accessTokenSecret, key, iv)
                }
            };

            db.Users.Add(twitterUser);

            var result = db.SaveChanges();             
            luxbsa.Hubs.Notification.Instance.UpdateStatistics();
            return result;
        }

    }
}
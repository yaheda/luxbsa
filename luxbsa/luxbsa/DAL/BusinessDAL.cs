﻿using luxbsa.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace luxbsa.DAL
{
    public static class BusinessDAL
    {
        public static int AddBusiness(luxbsaEntities db, Business business)
        {
            db.Businesses.Add(business);
            var result = db.SaveChanges();
            luxbsa.Hubs.Notification.Instance.UpdateStatistics();
            return result;
        }

        public static int RemoveBusiness(luxbsaEntities db, int businessId)
        {
            var business = db.Businesses.Where(e => e.Id == businessId).Single();
            db.Businesses.Remove(business);
            var result = db.SaveChanges();
            luxbsa.Hubs.Notification.Instance.UpdateStatistics();
            return result;
        }

        public static int ModifyBusiness(luxbsaEntities db, Business business)
        {
            Business businessEtity = GetById(db, business.Id);

            if (businessEtity == null)
                throw new Exception("Business for ID = " + business.Id + " does not exist.");

            db.Entry(businessEtity).CurrentValues.SetValues(business);


            foreach (User user in businessEtity.Users.ToList())
            {
                if (!business.Users.Any(e => e.Id == user.Id))
                    businessEtity.Users.Remove(user);
            }

            for (int i = 0; i < business.Users.Count; i++)
            {
                User user = business.Users.ElementAt(i);
                
                User userEntity = businessEtity.Users.Where(e => e.Id == user.Id).SingleOrDefault();
                if (userEntity == null)
                    businessEtity.Users.Add(user);
                else
                    db.Entry(userEntity).CurrentValues.SetValues(user);
            }
            
            return db.SaveChanges();
        }

        public static Business GetById(luxbsaEntities db, int id)
        {
            return db.Businesses.Where(e => e.Id == id).SingleOrDefault();
        }

        public static List<Business> SearchBusinessTags(luxbsaEntities db, string term)
        {
            return db.Businesses.Where(e => e.Name.ToLower().StartsWith(term.ToLower())).ToList();
        }

        public static List<Business> GetAll(luxbsaEntities db)
        {
            return db.Businesses.ToList();
        }

        public static int GetTotal(luxbsaEntities db)
        {
            return db.Businesses.Count();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace luxbsa.DAL
{
    public static class SectorDAL
    {
        public static Sector GetById(int id)
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                return db.Sectors.Where(e => e.Id == id).SingleOrDefault();
            }
        }

        public static List<Sector> GetAll()
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                return db.Sectors.ToList();
            }
        }
    }
}
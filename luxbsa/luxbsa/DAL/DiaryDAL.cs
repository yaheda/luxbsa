﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace luxbsa.DAL
{
    public class DiaryDAL
    {
        public static int AddDiary(luxbsaEntities db, Diary diary)
        {
            db.Diaries.Add(diary);
            var result = db.SaveChanges();
            luxbsa.Hubs.Notification.Instance.UpdateStatistics();
            return result;
        }

        public static int RemoveDiary(luxbsaEntities db, int diaryId)
        {
            var diary = db.Diaries.Where(e => e.Id == diaryId).Single();
            db.Diaries.Remove(diary);
            var result = db.SaveChanges();
            luxbsa.Hubs.Notification.Instance.UpdateStatistics();
            return result;
        }

        public static Diary GetDiaryById(luxbsaEntities db, int id)
        {
            return db.Diaries.Where(e => e.Id == id).SingleOrDefault();
        }

        public static List<Diary> GetDiaryEntriesByUserId(luxbsaEntities db, int userId)
        {
            return db.Diaries.Where(e => e.UserID == userId).ToList();
        }

        public static int GetTotal(luxbsaEntities db)
        {
            return db.Diaries.Count();
        }
    }
}
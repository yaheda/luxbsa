﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace luxbsa.DAL
{
    public static class ContactDAL
    {
        public static int AddContact(luxbsaEntities db, Contact contact)
        {
            db.Contacts.Add(contact);
            return db.SaveChanges();
        }
    }
}
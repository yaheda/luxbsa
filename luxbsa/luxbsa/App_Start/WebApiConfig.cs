﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Routing;

namespace luxbsa
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //config.Routes.MapHttpRoute(
            //   name: "Api_Get",
            //   routeTemplate: "api/{controller}/{action}/{id}",
            //   defaults: new { id = RouteParameter.Optional, action = "Get" },
            //   constraints: new { httpMethod = new HttpMethodConstraint("GET") }
            //);

            //config.Routes.MapHttpRoute(
            //   name: "Api_Post",
            //   routeTemplate: "api/{controller}/{action}/{id}",
            //   defaults: new { id = RouteParameter.Optional, action = "Post" },
            //   constraints: new { httpMethod = new HttpMethodConstraint("POST") }
            //);
        }
    }
}

﻿using System;
using System.Web;
using System.Web.Optimization;

namespace luxbsa
{
    public class BundleConfig
    {

        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {

            #region OLD

            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            //            "~/Scripts/jquery-ui-{version}.js"));



            bundles.Add(new StyleBundle("~/StyleBundle/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));


            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            #endregion

            #region TOP

            bundles.Add(new StyleBundle("~/StyleBundle/Bootstrap")
                .Include("~/Content/css/bootstrap.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/bootstrap-tagsinput.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/font-awesome.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/StyleBundle/css")
                .Include("~/Content/site.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/StyleBundle/JQuery-ui")
                .Include("~/Content/css/ui-flick/jquery-ui-1.10.4.custom.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/jquery.datetimepicker.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/StyleBundle/Fonts")
                .Include("~/Content/css/fonts.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/StyleBundle/Plugin")
                .Include("~/Content/plugins/prettyPhoto-plugin/css/prettyPhoto.css", new CssRewriteUrlTransform())
                .Include("~/Content/plugins/isotope-plugin/css/isotope.css", new CssRewriteUrlTransform())
                .Include("~/Content/plugins/rs-plugin/css/settings.css", new CssRewriteUrlTransform())
                .Include("~/Content/plugins/google-code-prettify/prettify.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/animate.min.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/StyleBundle/Theme")
                .Include("~/Content/css/theme.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/chrome.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/header/header-3.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/StyleBundle/IE")
                .Include("~/Content/css/ie.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/StyleBundle/DesignMode")
                .Include("~/Content/css/mode/mode-1-header.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/mode/mode-3-footer.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/color/color-blue.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/StyleBundle/Responsive")
                .Include("~/Content/css/theme-responsive.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/header/header-3-responsive.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/mode/mode-1-header-responsive.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/mode/mode-3-footer-responsive.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/color/color-blue-responsive.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/StyleBundle/Custom")
                .Include("~/Content/custom/custom.css", new CssRewriteUrlTransform()));

            bundles.Add(new ScriptBundle("~/js/Modernizr").Include(
                "~/Content/js/modernizr.js"));

            bundles.Add(new ScriptBundle("~/js/IE8HTML5").Include(
                "~/Content/js/html5shiv.js",
                "~/Content/js/respond.min.js"));

            bundles.Add(new ScriptBundle("~/js/JQueryVal").Include(
                "~/Content/js/jquery.unobtrusive*",
                "~/Content/js/jquery.validate*"));

            #endregion

            #region BOTTOM

            bundles.Add(new ScriptBundle("~/js/Global").Include(
                "~/Content/js/jquery.js",     
                "~/Content/js/bootstrap.js",
                "~/Content/js/specific/bootstrap-tagsinput.js",
                "~/Content/js/jquery-ui.js",
                "~/Content/js/jquery.datetimepicker.js",
                "~/Content/js/tinyscrollbar.js",
                "~/Content/js/caroufredsel.js",
                "~/Content/js/plugins.js",
                "~/Content/plugins/prettyPhoto-plugin/js/jquery.prettyPhoto.js",
                "~/Content/plugins/isotope-plugin/js/jquery.isotope.js",
                "~/Content/functions/twitter/jquery.tweet.js",
                "~/Content/js/theme.js",
                "~/Content/js/ICanHaz.js",
                "~/Content/js/readmore.js",
                "~/Content/custom/custom.js"));

            bundles.Add(new ScriptBundle("~/js/SignalR").Include(
               "~/Scripts/jquery.signalR-2.0.2.js",
               "~/signalr/hubs"));

            bundles.Add(new ScriptBundle("~/js/Specific/MetroSlider").Include("~/Content/js/specific/metro-slider.js"));
            bundles.Add(new ScriptBundle("~/js/Specific/QuickContact").Include("~/Content/js/specific/quick-contact.js"));

            bundles.Add(new ScriptBundle("~/js/Specific/Classie").Include("~/Content/js/specific/classie.js"));

            bundles.Add(new ScriptBundle("~/js/Specific/Typeahead").Include("~/Content/js/specific/typeahead.js"));

            #endregion
        }
    }
}
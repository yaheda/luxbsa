﻿using AutoMapper;
using luxbsa.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace luxbsa
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            /// Register Automapper
            Mapper.CreateMap<UserModel, User>();
            Mapper.CreateMap<User, UserModel>();

            Mapper.CreateMap<BusinessModel, Business>();
            Mapper.CreateMap<Business, BusinessModel>();

            //Mapper.CreateMap<Procedure_GetDiariesByUserIdAndStepAndCount_Result, Diary>();
            //Mapper.CreateMap<Diary, Procedure_GetDiariesByUserIdAndStepAndCount_Result>();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace luxbsa.Exceptions
{
    [Serializable]
    public class UniqueValueException : Exception
    {
        public UniqueValueException()
            : base() { }

        public UniqueValueException(string message)
            : base(message) { }

        public UniqueValueException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public UniqueValueException(string message, Exception innerException)
            : base(message, innerException) { }

        public UniqueValueException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

        protected UniqueValueException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
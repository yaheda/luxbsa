//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace luxbsa
{
    using System;
    using System.Collections.Generic;
    
    public partial class User
    {
        public User()
        {
            this.Diaries = new HashSet<Diary>();
            this.Businesses = new HashSet<Business>();
        }
    
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public Nullable<int> Gender { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string AboutMe { get; set; }
        public Nullable<int> TwitterAuthId { get; set; }
        public int Active { get; set; }
        public string ActivationToken { get; set; }
    
        public virtual ICollection<Diary> Diaries { get; set; }
        public virtual TwitterAuth TwitterAuth { get; set; }
        public virtual ICollection<Business> Businesses { get; set; }
    }
}

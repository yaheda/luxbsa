﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(luxbsa.Startup))]
namespace luxbsa
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
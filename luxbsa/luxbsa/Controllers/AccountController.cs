﻿using AutoMapper;
using luxbsa.DAL;
using luxbsa.Helpers;
using luxbsa.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace luxbsa.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        [AllowAnonymous]
        public ActionResult Login()
        {
            var model = TempData[Constants.Instance.KEY_LoginModel] as LoginModel;

            if (model == null)
                model = new LoginModel();
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult AdminLogin()
        {
            var model = new LoginModel() { IsBeta = false };
            TempData[Constants.Instance.KEY_LoginModel] = model;
            return View("Login", model);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                User user = UserDAL.GetByEmailAndPassword(model.Email, model.Password);

                if (user == null)
                    ModelState.AddModelError("", "Invalid Email or Password");
                else if (user.Active == (int)UserModel.ActivationStatus.InActive)
                {
                    ///TODO: add activate link here
                    ModelState.AddModelError("", "You need to activate your account in order to use it");
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(user.Id.ToString(), true);
                    Session[Constants.Instance.KEY_User_Id] = user.Id;
                    Session[Constants.Instance.KEY_User_Firstname] = user.Firstname;
                    Session[Constants.Instance.KEY_User_Surname] = user.Surname;
                    return RedirectToAction("index", "home");
                }

            }

            var modelValue = TempData[Constants.Instance.KEY_LoginModel] as LoginModel;
            if (modelValue == null)
                modelValue = new LoginModel();

            return View(modelValue);
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Register(UserModel model)
        {
            if (ModelState.IsValid)
            {
                using (luxbsaEntities db = new luxbsaEntities())
                {
                    User user = Mapper.Map<User>(model);
                    int result = UserDAL.AddUser(db, user);
                    return RedirectToAction("login", "account");
                }
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult Activate(string token)
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                var model = new LoginModel();
                User user = UserDAL.GetByActivationToken(db, token);

                if (user == null)
                {
                    model.ShowActivationCompleted = false;
                    return View("Login", model);
                }

                UserDAL.ActivateUserByActivationToken(db, token);
                model.ShowActivationCompleted = true;

                return View("Login", model);
            }
        }

        public ActionResult Deactivate(string token)
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                var model = new LoginModel();
                User user = UserDAL.GetByActivationToken(db, token);

                if (user == null)
                {
                    model.ShowDeactivationCompleted = false;
                    return View("Login", model);
                }

                UserDAL.DeactivateUserByActivationToken(db, token);
                model.ShowDeactivationCompleted = true;

                return View("Login", model);
            }
        }

        [AllowAnonymous]
        public ActionResult Disassociate(string token)
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                var model = new LoginModel();
                User user = UserDAL.GetByActivationToken(db, token);

                if (user == null)
                {
                    model.ShowDisassociationCompleted = false;
                }

                if (user.Active == (int)UserModel.ActivationStatus.InActive)
                {
                    UserDAL.RemoveByActivationToken(db, token);
                    model.ShowDisassociationCompleted = true;
                }
                else
                {
                    model.ShowDisassociationCompleted = false;
                }

                return View("Login", model);
            }
        }

        [AllowAnonymous]
        public ActionResult Terms()
        {
            return View("Login", new LoginModel() { ShowTerms = true });
        }


        public ActionResult Logout()
        {
            //var x = User.Identity.Name;
            FormsAuthentication.SignOut();
            Session[Constants.Instance.KEY_User_Id] = null;
            Session[Constants.Instance.KEY_User_Firstname] = null;
            Session[Constants.Instance.KEY_User_Surname] = null;
            return RedirectToAction("login", "account");
        }

    }
}

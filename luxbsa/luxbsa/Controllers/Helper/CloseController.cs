﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace luxbsa.Controllers.Helper
{
    [AllowAnonymous]
    public class CloseController : Controller
    {
        public ActionResult Close()
        {
            return View();
        }

    }
}

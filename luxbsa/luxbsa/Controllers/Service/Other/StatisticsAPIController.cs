﻿using luxbsa.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace luxbsa.Controllers.Service.Other
{
    [AllowAnonymous]
    public class StatisticsAPIController : Controller
    {
        [HttpGet]
        public JsonResult GetNumberOfUsers()
        {
            using (luxbsaEntities db = new luxbsaEntities())
            { 
                return new JsonResult() { Data = UserDAL.GetTotal(db), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpGet]
        public JsonResult GetNumberOfBusinesses()
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                return new JsonResult() { Data = BusinessDAL.GetTotal(db), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpGet]
        public JsonResult GetNumberOfDiaryEntries()
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                return new JsonResult() { Data = DiaryDAL.GetTotal(db), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }
    }
}

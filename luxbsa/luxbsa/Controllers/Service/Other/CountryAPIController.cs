﻿using luxbsa.CountryInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace luxbsa.Controllers.Service
{
    public class CountryAPIController : Controller
    {
        [HttpGet]
        public static JsonResult ListOfCountryNamesByName()
        {
            CountryInfoService service = new CountryInfoService();
            tCountryCodeAndName[] result = new List<tCountryCodeAndName>().ToArray();            
            tCountryCodeAndNameGroupedByContinent[] countinentList = service.ListOfCountryNamesGroupedByContinent();
            foreach (tCountryCodeAndNameGroupedByContinent coutinent in countinentList)
            {
                if (coutinent.Continent.sName.Trim().ToLower() == "africa")
                {
                    result = coutinent.CountryCodeAndNames;
                    break;
                }
            }
            //tCountryCodeAndName[] result = service.ListOfCountryNamesByName();
            return new JsonResult() { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpGet]
        public static JsonResult FullCountryInfoAllCountries()
        {
            CountryInfoService service = new CountryInfoService();
            tCountryInfo[] countryInfoList = service.FullCountryInfoAllCountries();
            return new JsonResult() { Data = countryInfoList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

    }
}

﻿using luxbsa.DAL;
using luxbsa.Helpers;
using luxbsa.Models.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace luxbsa.Controllers.Service
{
    [Authorize]
    public class ContactAPIController : Controller
    {
        [AllowAnonymous]
        [HttpPost]
        public JsonResult SendMessage(string data)
        {
            luxbsa.Hubs.Notification.Instance.UpdateStatistics();
            using (luxbsaEntities db = new luxbsaEntities())
            {
                dynamic oData = JsonConvert.DeserializeObject(data);

                Contact contact = new Contact()
                {
                    Name = oData.Name,
                    Email = oData.Email,
                    Reason = Convert.ToInt32(oData.Reason),
                    Subject = oData.Subject,
                    Message = oData.Message
                };

                ContactDAL.AddContact(db, contact);

                var subject = contact.Subject;
                var body =
                    "<div><u>Name:</u> " + contact.Name + "</div>" +
                    "<div><u>Reason:</u> " + ((ContactReasonEnum)contact.Reason).ToString() + "</div><br />" +
                    "<div><u>Content:</u></div>" +
                    "<div>" +contact.Message + "</div>";

                var sendtoEmail = ConfigurationManager.AppSettings[Constants.Instance.KEY_LuxbsaEmailAddress_Info];
                if (Constants.Instance.Debug == true) sendtoEmail = "yaheda.a@gmail.com";
                EmailHelper.SendEmail(new MailAddress(contact.Email, contact.Name), Constants.Instance.LUXBSA_MailAddress, "LUX-BSA " + ((ContactReasonEnum)contact.Reason).ToString() + ": " +subject, body);
                
                return new JsonResult() { Data = "", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }
    }
}

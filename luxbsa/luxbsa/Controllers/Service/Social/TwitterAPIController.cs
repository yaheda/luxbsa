﻿//extern alias twitter_alias;
//using Twitter = twitter_alias::Tweetinvi;

using luxbsa.DAL;
using luxbsa.Helpers;
using luxbsa.Hubs;
using luxbsa.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using TweetSharp;

namespace luxbsa.Controllers
{
    [AllowAnonymous]
    public class TwitterAPIController : Controller
    {
        private static readonly ConcurrentDictionary<int, TwitterService> twitterServiceConnections = new ConcurrentDictionary<int, TwitterService>();
        private static readonly Dictionary<int, bool> twitterSteam = new Dictionary<int, bool>();

        private Uri GetTwitterAuthUri()
        {
            TwitterService service = new TwitterService(
                    ConfigurationManager.AppSettings[Constants.Instance.KEY_Twitter_ConsumerKey],
                    ConfigurationManager.AppSettings[Constants.Instance.KEY_Twitter_ConsumerSecret]);
            OAuthRequestToken requestToken = service.GetRequestToken("http://" + Constants.Instance.Domain + "/TwitterAPI/AuthorizeCallback");

            Uri uri = service.GetAuthorizationUri(requestToken);
            return uri;
        }

        //[HttpPost]
        //public JsonResult AuthorizeJson()
        //{
        //    Uri uri = GetTwitterAuthUri();
        //    return new JsonResult() { Data = uri.ToString(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        //}

        [HttpPost]
        public JsonResult AuthorizeJson(string data)
        {
            if (data != null)
            {
                dynamic oData = JsonConvert.DeserializeObject(data);
                string email = oData.Email;
                string connectionId = oData.ConnectionId;
               
                HttpContext.Session[Constants.Instance.KEY_Twitter_Notication_ConnectionId] = connectionId;
                HttpContext.Session[Constants.Instance.KEY_User_Email] = email;
            }

            Uri uri = GetTwitterAuthUri();
            return new JsonResult() { Data = uri.ToString(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        // This URL is registered as the application's callback at http://dev.twitter.com
        public ActionResult AuthorizeCallback(string oauth_token, string oauth_verifier)
        {

            var userIdSession = HttpContext.Session[Constants.Instance.KEY_User_Id];

            if (userIdSession == null)
            {
                if (oauth_token == null || oauth_verifier == null)
                {
                    /// TODO: notify error
                }
                else
                {
                    var requestToken = new OAuthRequestToken { Token = oauth_token };
                    TwitterService service = new TwitterService(
                        ConfigurationManager.AppSettings[Constants.Instance.KEY_Twitter_ConsumerKey],
                        ConfigurationManager.AppSettings[Constants.Instance.KEY_Twitter_ConsumerSecret]);
                    OAuthAccessToken accessToken = service.GetAccessToken(requestToken, oauth_verifier);
                    service.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);
                    TwitterUser twitterUser = service.VerifyCredentials(new VerifyCredentialsOptions());
                    
                    if (twitterUser != null)
                    {
                        using (luxbsaEntities db = new luxbsaEntities())
                        {
                            string email = HttpContext.Session[Constants.Instance.KEY_User_Email].ToString();
                            string name = twitterUser.Name;
                            string activationToken = Guid.NewGuid().ToString();

                            UserDAL.AddTwitterUser(db, email, name, activationToken, accessToken.Token, accessToken.TokenSecret);

                            if (Constants.Instance.Debug) email = "yaheda.a@gmail.com";
                            Email oEmail = EmailHelper.CreateRegistrationEmail(name, activationToken);
                            EmailHelper.SendEmail(Constants.Instance.LUXBSA_MailAddress, new MailAddress(email, name), oEmail.Subject, oEmail.Body);

                            Notification.Instance.SendTwitterSignUp_SuccessNotification();
                        }
                    }
                    else
                    {
                        /// TODO: notify error
                    }
                }
            }

            TempData[Constants.Instance.KEY_LoginModel] = new LoginModel() { SignUpCompleted = true};
            return RedirectToAction("Login", "Account");

            /// TODO: scrap tweets
            //int userId = Convert.ToInt32(userIdSession);

            //if (oauth_token == null || oauth_verifier == null)
            //{
            //    Notification.Instance.SendTwitterAuthorizeCallbackFailureNofication(userId.ToString());
            //}
            //else
            //{
            //    var requestToken = new OAuthRequestToken { Token = oauth_token };

            //    TwitterService service = new TwitterService(
            //        ConfigurationManager.AppSettings[Constants.Twitter_ConsumerKey],
            //        ConfigurationManager.AppSettings[Constants.Twitter_ConsumerSecret]);
            //    OAuthAccessToken accessToken = service.GetAccessToken(requestToken, oauth_verifier);

            //    service.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);

            //    service = twitterServiceConnections.GetOrAdd(userId, service);

            //    TwitterUser user = service.VerifyCredentials(new VerifyCredentialsOptions());
                
            //    if (user != null)
            //    {
            //        using (luxbsaEntities db = new luxbsaEntities())
            //        {
            //            UserDAL.AddTwitterAuth(db, userId, accessToken.Token, accessToken.TokenSecret);
            //            Notification.Instance.SendTwitterAuthorizeCallbackSuccessNofication(userId.ToString());
            //        }

            //        StreamFromUserStream(service);
            //    }
            //    else
            //    {
            //        Notification.Instance.SendTwitterAuthorizeCallbackFailureNofication(userId.ToString());
            //    }
            //}            
        }

        public static TwitterService GetTwitterService(luxbsaEntities db, int userId)
        {
            TwitterAuth twitterAuth = UserDAL.GetTwitterAuth(db, userId);
            TwitterService service = new TwitterService(
                    ConfigurationManager.AppSettings[Constants.Instance.KEY_Twitter_ConsumerKey],
                    ConfigurationManager.AppSettings[Constants.Instance.KEY_Twitter_ConsumerSecret]);

            var key = ConfigurationManager.AppSettings[Constants.Instance.KEY_SecurityKey];
            var iv = ConfigurationManager.AppSettings[Constants.Instance.KEY_SecurityIV];

            string decryptedAccessToken = SecurityHelper.Decrypt(twitterAuth.AccessToken, key, iv);
            string decryptedAccessTokenSecret = SecurityHelper.Decrypt(twitterAuth.AccessTokenSecret, key, iv);

            service.AuthenticateWith(decryptedAccessToken, decryptedAccessTokenSecret);

            return twitterServiceConnections.GetOrAdd(userId, service);
        }

        [HttpPost]
        public void OpenTwitterStreamForUser(string userId)
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                int _userId = Convert.ToInt32(userId);
                TwitterService service = GetTwitterService(db, _userId);
                
                bool isStreaming;
                if (!twitterSteam.TryGetValue(_userId, out isStreaming))
                {
                    StreamFromUserStream(service);
                    twitterSteam.Add(_userId, true);
                }
            }
        }

        private void StreamFromUserStream(TwitterService service)
        {
            service.StreamUser((streamEvent, response) =>
            {
                if (streamEvent is TwitterUserStreamEnd)
                {
                    //block.Set();
                }

                if (response.StatusCode == 0)
                {
                    if (streamEvent is TwitterUserStreamFriends)
                    {
                        var friends = (TwitterUserStreamFriends)streamEvent;
                    }

                    if (streamEvent is TwitterUserStreamEvent)
                    {
                        var @event = (TwitterUserStreamEvent)streamEvent;
                    }

                    if (streamEvent is TwitterUserStreamStatus)
                    {
                        var tweet = ((TwitterUserStreamStatus)streamEvent).Status;
                    }

                    if (streamEvent is TwitterUserStreamDeleteStatus)
                    {
                        var deleted = (TwitterUserStreamDeleteStatus)streamEvent;
                    }
                }
                else
                {
                    //Assert.Ignore("Stream responsed with status code: {0}", response.StatusCode);
                }
            });

            //block.WaitOne();
            //service.CancelStreaming();
        }
    }
}

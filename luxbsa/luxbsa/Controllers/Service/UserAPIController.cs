﻿using luxbsa.DAL;
using luxbsa.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TweetSharp;

namespace luxbsa.Controllers.Service
{
    [Authorize]
    public class UserAPIController : Controller
    {
        [HttpGet]
        public JsonResult IsTwitterValid()
        {
            int userId = Convert.ToInt32(HttpContext.Session[Constants.Instance.KEY_User_Id]);

            using (luxbsaEntities db = new luxbsaEntities())
            {
                TwitterAuth twitterAuth = UserDAL.GetTwitterAuth(db, userId);

                if (twitterAuth == null)
                    return new JsonResult() { Data = false, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                else
                {
                    TwitterService service = new TwitterService(
                    ConfigurationManager.AppSettings[Constants.Instance.KEY_Twitter_ConsumerKey],
                    ConfigurationManager.AppSettings[Constants.Instance.KEY_Twitter_ConsumerSecret]);

                    var key = ConfigurationManager.AppSettings[Constants.Instance.KEY_SecurityKey];
                    var iv = ConfigurationManager.AppSettings[Constants.Instance.KEY_SecurityIV];

                    string decryptedAccessToken = SecurityHelper.Decrypt(twitterAuth.AccessToken, key, iv);
                    string decryptedAccessTokenSecret = SecurityHelper.Decrypt(twitterAuth.AccessTokenSecret, key, iv);

                    service.AuthenticateWith(decryptedAccessToken, decryptedAccessTokenSecret);
                    TwitterUser user = service.VerifyCredentials(new VerifyCredentialsOptions());

                    if (user == null)
                        return new JsonResult() { Data = false, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                    else
                        return new JsonResult() { Data = true, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public JsonResult IsEmailAvailable(string email)
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                var list = UserDAL.GetUsersByEmail(db, email);
                
                if (list.Count == 0)
                    return new JsonResult() { Data = false, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                else
                    return new JsonResult() { Data = true, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

    }
}

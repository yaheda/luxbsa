﻿using luxbsa.DAL;
using luxbsa.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace luxbsa.Controllers
{
    [Authorize]
    public class AccountAPIController : Controller
    {
        [AllowAnonymous]
        public JsonResult RegisterUser(string data)
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                dynamic oData = JsonConvert.DeserializeObject(data);

                var activationToken = Guid.NewGuid().ToString();
                User user = new User()
                {
                    Firstname = oData.Firstname,
                    Surname = oData.Surname,
                    Email = oData.Email,
                    Password = oData.Password,
                    ActivationToken = activationToken
                };

                UserDAL.AddUser(db, user);

                //TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                //var subject = "LUX BSA Registration Verification";
                //var body =
                //    "Hi " + textInfo.ToTitleCase(user.Firstname) + "," + Environment.NewLine +
                //    "To complete the registration process, please click on the link below." + Environment.NewLine +
                //    Constants.LuxbsaActivate_Link + "?token=" + activationToken.ToString() + Environment.NewLine +
                //    "Kind Regards from the LUX BSA team";

                if (Constants.Instance.Debug) user.Email = "yaheda.a@gmail.com";
                Email oEmail = EmailHelper.CreateRegistrationEmail(user.Firstname, activationToken);                
                EmailHelper.SendEmail(Constants.Instance.LUXBSA_MailAddress, new MailAddress(user.Email, user.Firstname + " " + user.Surname), oEmail.Subject, oEmail.Body);

                return new JsonResult() { Data = "", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

    }
}

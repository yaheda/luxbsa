﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using luxbsa.DAL;
using System.Data.Entity.Validation;
using luxbsa.Helpers;
using luxbsa.Models;
using System.Data.Objects;
using AutoMapper;

namespace luxbsa.Controllers
{
    [Authorize]
    public class DiaryAPIController : Controller
    {
        [HttpPost]
        public JsonResult AddDiaryEntry(string data)
        {
            try
            {
                using (luxbsaEntities db = new luxbsaEntities())
                {
                    dynamic oData = JsonConvert.DeserializeObject(data);

                    string title = oData.Title;
                    JArray jBusinesses = oData.Businesses;
                    JArray jTags = oData.Tags;
                    string content = oData.Content;
                    string jDateCreated = oData.DateCreated;

                    Diary diary = new Diary();
                    diary.UserID =  Convert.ToInt32(HttpContext.Session[Constants.Instance.KEY_User_Id]);
                    diary.Title = title;
                    diary.Content = content;
                    diary.DateCreated = JavaScriptDateConverter.Convert(Convert.ToInt64(jDateCreated));

                    if (jBusinesses != null)
                    {
                        for (int i = 0; i < jBusinesses.Count; i++)
                        {
                            dynamic jBusiness = jBusinesses.ElementAt(i);
                            int businessId = Convert.ToInt32(jBusiness.key);

                            Business business = BusinessDAL.GetById(db, businessId);
                            diary.Businesses.Add(business);
                        }
                    }

                    if (jTags != null)
                    {
                        for (int i = 0; i < jTags.Count; i++)
                        {
                            string tagText = jTags.ElementAt(i).ToString();

                            Tag tag = TagDAL.GetTagByText(db, tagText);
                            if (tag == null)
                            {
                                tag = new Tag() { Text = tagText };
                                TagDAL.AddTag(db, tag);
                            }

                            diary.Tags.Add(tag);
                        }
                    }

                    int result = DiaryDAL.AddDiary(db, diary);

                    return new JsonResult() { Data = "", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }

                throw ex;
            }
        }

        [HttpPost]
        public JsonResult GetDiaryEntriesForUser(string data)
        {
            dynamic oData = JsonConvert.DeserializeObject(data);

            string jUserId = oData.userId;
            string jStep = oData.step;
            string jCount = oData.count;

            using (luxbsaEntities db = new luxbsaEntities())
            {
                var diaryResults = db.Procedure_GetDiariesByUserIdAndStepAndCount(Convert.ToInt32(jUserId), Convert.ToInt32(jStep), Convert.ToInt32(jCount)).ToList();
                //List<Diary> diaryEntries = DiaryDAL.GetDiaryEntriesByUserId(db, Convert.ToInt32(jUserId));
                List<dynamic> diaryModels = new List<dynamic>();
                for (int i = 0; i < diaryResults.Count; i++)
                {
                    var diaryResult = diaryResults.ElementAt(i);
                    Diary diary = DiaryDAL.GetDiaryById(db, diaryResult.Id); //Mapper.Map<Diary>(diaryResult);
                    DiaryModel diaryModel = new DiaryModel(diary);
                    diaryModels.Add(diaryModel);
                }

                var loadmore = (Convert.ToInt32(jCount) + (Convert.ToInt32(jCount) * Convert.ToInt32(jStep)) < db.Diaries.Count());

                return new JsonResult() 
                { 
                    Data = new 
                    {
                        diaries = diaryModels,
                        loadmore = loadmore
                    }, 
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet 
                };
            }
        }
    }
}

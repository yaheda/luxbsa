﻿using luxbsa.DAL;
using luxbsa.Helpers;
using luxbsa.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace luxbsa.Controllers
{
    [Authorize]
    public class BusinessAPIController : Controller
    {
        [HttpGet]
        public JsonResult SearchBusinessTags(string term)
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                List<TypeAheadModel> results = new List<TypeAheadModel>();
                List<Business> searchResults = BusinessDAL.SearchBusinessTags(db, term);
                foreach (Business business in searchResults)
                {
                    TypeAheadModel typeAhead = new TypeAheadModel();
                    typeAhead.key = business.Id.ToString();
                    typeAhead.value = FormatHelper.GetTypeAheadBusiness(business);
                    results.Add(typeAhead);
                }
                return new JsonResult() { Data = results.ToArray(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpGet]
        public JsonResult GetBusinessItems()
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                List<object> results = new List<object>();
                List<Business> businessEntityList = BusinessDAL.GetAll(db);
                foreach (Business business in businessEntityList)
                {
                    var businessObject = new
                    {
                        Id = business.Id,
                        Name = business.Name,
                        Description = business.Description,
                        VideoLink = business.VideoLink,
                        HasVideoLink = business.VideoLink != null ? true : false,
                        Country = business.Country,
                        Sector = new
                        {
                            Id = business.Sector.Id,
                            Name = business.Sector.Name
                        } 
                    };
                    results.Add(businessObject);
                }
                return new JsonResult() { Data = results.ToArray(), JsonRequestBehavior = JsonRequestBehavior.AllowGet }; 
            }
        }
    }
}

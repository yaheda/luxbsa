﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Newtonsoft.Json;
using luxbsa.DAL;
using luxbsa.Models;

namespace luxbsa.Controllers
{
    [Authorize]
    public class TagAPIController : Controller
    {
        [HttpGet]
        public JsonResult SearchTags(string term)
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                List<TypeAheadModel> results = new List<TypeAheadModel>();
                List<Tag> searchResults = TagDAL.SearchTags(db, term);
                foreach (Tag tag in searchResults)
                {
                    TypeAheadModel typeAhead = new TypeAheadModel();
                    typeAhead.key = tag.Id.ToString();
                    typeAhead.value = tag.Text;
                    results.Add(typeAhead);
                }
                return new JsonResult() { Data = results.ToArray(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }
    }
}

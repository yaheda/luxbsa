﻿using luxbsa.DAL;
using luxbsa.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace luxbsa.Controllers
{
    [Authorize]
    public class SectorAPIController : Controller
    {
        [HttpGet]
        public static JsonResult GetSectors()
        {
            var sectors = new List<SectorModel>();
            var sectorEntityList = SectorDAL.GetAll();

            foreach (Sector sectorEntity in sectorEntityList)
            {
                var sectorModel = new SectorModel(sectorEntity);
                sectors.Add(sectorModel);
            }

            return new JsonResult() { Data = sectors.OrderBy(o => o.Name), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}

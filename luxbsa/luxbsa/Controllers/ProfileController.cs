﻿using AutoMapper;
using luxbsa.DAL;
using luxbsa.Exceptions;
using luxbsa.Helpers;
using luxbsa.Models;
using Newtonsoft.Json;
using Omu.AwesomeMvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace luxbsa.Controllers
{
    [Authorize]
    [BaseSessionFilterAttribute]
    public class ProfileController : Controller
    {
        #region User Profile

        public ActionResult UserProfile()
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                User user = UserDAL.GetById(db, Convert.ToInt32(User.Identity.Name));
                UserModel model = Mapper.Map<UserModel>(user);

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult UserProfile(UserModel model)
        {
            return View();
        }


        #endregion

        #region Business Profile

        public ActionResult BusinessProfiles()
        {
            using (luxbsaEntities db = new luxbsaEntities())
            {
                User user = UserDAL.GetById(db, Convert.ToInt32(User.Identity.Name));
                UserModel model = new UserModel(user);
                model.Businesses = model.Businesses.ToList();
                return View(model);
            }
        }

        public ActionResult ViewBusinessProfile(String id)
        {
            return View();
        }

        public ActionResult BusinessProfile(String id)
        {
            BusinessModel model = null;

            if (id == null)
                model = new BusinessModel();
            else
            {
                using (luxbsaEntities db = new luxbsaEntities())
                {
                    Business business = BusinessDAL.GetById(db, Convert.ToInt32(id));
                    model = Mapper.Map<BusinessModel>(business);

                    model.Members = new List<TypeAheadModel>();
                    foreach (User user in business.Users)
                    {
                        TypeAheadModel typeAhead = new TypeAheadModel();
                        typeAhead.key = user.Id.ToString();
                        typeAhead.value = user.Firstname + " " + user.Surname;
                        model.Members.Add(typeAhead);
                    }
                }
            }

            return View(model);
        }

        //[HttpPost]
        //[MultipleButton(Name = "action", Argument = "save")]
        public ActionResult SaveBusinessProfile(BusinessModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (luxbsaEntities db = new luxbsaEntities())
                    {
                        Business business = model.Entity;

                        business.Users.Clear();
                        foreach (TypeAheadModel item in model.Members)
                        {
                            if (item.delete != "true")
                            {
                                User user = UserDAL.GetById(db, Convert.ToInt32(item.key));
                                business.Users.Add(user);
                            }
                        }

                        if (business.Id == 0)
                            BusinessDAL.AddBusiness(db, business);
                        else
                            BusinessDAL.ModifyBusiness(db, business);
                        
                        return RedirectToAction("ViewBusinessProfiles", "Profile");
                    }
                }
                catch (UniqueValueException ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            return new EmptyResult();
        }

        #endregion

        #region AJAX

        [HttpGet]
        public JsonResult GetUsers(string term)
        {
            List<TypeAheadModel> results = new List<TypeAheadModel>();
            List<User> searchResults = UserDAL.SearchUser(term);

            foreach (User user in searchResults)
            {
                TypeAheadModel typeAhead = new TypeAheadModel();
                typeAhead.key = user.Id.ToString();
                typeAhead.value = user.Firstname + " " + user.Surname;
                results.Add(typeAhead);
            }

            return new JsonResult()
            {
                Data = results.ToArray(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult GetUserById(string id)
        {
            try
            {
                int userId = Convert.ToInt32(id);
                using (luxbsaEntities db = new luxbsaEntities())
                {
                    User user = UserDAL.GetById(db, userId);
                    TypeAheadModel typeAhead = new TypeAheadModel();
                    typeAhead.key = user.Id.ToString();
                    typeAhead.value = user.Firstname + " " + user.Surname;

                    return new JsonResult() { Data = typeAhead, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
            }
            catch (Exception ex)
            {
                return new JsonResult() { Data = ex.Message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpPost]
        public JsonResult AddDiaryEntry(string data) 
        {
            try
            {
                dynamic oData = JsonConvert.DeserializeObject(data);

                var title = oData.title;
                var tags = oData.tags;
                var content = oData.content;

                
                return new JsonResult() { Data = "hello", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (Exception ex)
            {
                return new JsonResult() { Data = ex.Message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        #endregion
    }
}

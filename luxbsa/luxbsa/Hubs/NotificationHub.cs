﻿using luxbsa.Helpers;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace luxbsa.Hubs
{
    public class NotificationHub : Hub
    {
        private readonly Notification _notification;
        private readonly ConnectionMapping<string> _connections;

        public NotificationHub() : this(Notification.Instance, Notification.ConnectionMapping) { }

        public NotificationHub(Notification notification, ConnectionMapping<string> connections)
        {
            _notification = notification;
            _connections = connections;
        }

        public async Task Subscribe(string userId)
        {
            await Groups.Add(Context.ConnectionId, userId);
        }

        public async Task Unsubscribe(string userId)
        {
            await Groups.Remove(Context.ConnectionId, userId);
        }

        public void Init()
        {
            /// TODO: encrypt connection id
            Clients.Caller.GetConnectionId(Context.ConnectionId);
        }

        public override Task OnConnected()
        {
            //string userId = HttpContext.Current.Session[Constants.User_Id].ToString();
            //_connections.Add(userId, Context.ConnectionId); 
            return base.OnConnected();
        }

        public override Task OnDisconnected()
        {
            //string userId = HttpContext.Current.Session[Constants.User_Id].ToString();
            //_connections.Remove(userId, Context.ConnectionId);
            return base.OnDisconnected();
        }

        public override Task OnReconnected()
        {
            //string userId = HttpContext.Current.Session[Constants.User_Id].ToString();
            //if (!_connections.GetConnections(userId).Contains(Context.ConnectionId))
            //{
            //    _connections.Add(userId, Context.ConnectionId);
            //}
            return base.OnReconnected();
        }
    }
}
﻿using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using System;

using System.Collections.Generic;
using luxbsa.Helpers;
using System.Linq;

namespace luxbsa.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        public void Send(string nTab, string roomName, string name, string message)
        {
            Clients.Caller.addChatMessage(nTab, name, message);
            Clients.OthersInGroup(roomName).addBroadcastChatMessage(nTab, name, message);
        }

        public void CreateSession(string roomName, string name, List<string> members)
        {
            string callerUserId = HttpContext.Current.Session[Constants.Instance.KEY_User_Id].ToString();
            foreach (var connectionId in Notification.ConnectionMapping.GetConnections(callerUserId))
            {
                ///Join room
                ///...
            }

            foreach (string userId in members)
            {
                ///send email here
                ///...
                
                if (Notification.ConnectionMapping.GetConnections(userId).Count > 0)
                {                
                    foreach (var connectionId in Notification.ConnectionMapping.GetConnections(userId))
                    {
                        //Clients.Client(connectionId).addChatMessage(name + ": " + message);
                        //Group
                    }
                }           
            }            
        }

        public async Task JoinRoom(string nTab, string roomName, string name)
        {
            await Groups.Add(Context.ConnectionId, roomName);
            var currentDate = DateTime.Now;

            string time = string.Empty;
            string hour = currentDate.Hour.ToString(); ;
            if (hour.Length == 1) hour = "0" + hour;
            string minute = currentDate.Minute.ToString();
            if (minute.Length == 1) minute = "0" + minute;

            time = hour + ":" + minute;
            
            Clients.Caller.addNotificationMessage(nTab, "You joined", time);     
            Clients.OthersInGroup(roomName).addNotificationMessage(nTab, name + " joined", time);
        }

        public async Task LeaveRoom(string roomName, string name)
        {
            await Groups.Remove(Context.ConnectionId, roomName);
            Clients.Group(roomName).addChatMessage(name + " left");
        }
    }
}
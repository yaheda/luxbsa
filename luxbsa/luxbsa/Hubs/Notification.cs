﻿using luxbsa.Helpers;
using luxbsa.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace luxbsa.Hubs
{
    public class Notification
    {
        private readonly static ConnectionMapping<string> _connections = new ConnectionMapping<string>();

        private readonly static Lazy<Notification> _instance = new Lazy<Notification>(() 
            => new Notification(GlobalHost.ConnectionManager.GetHubContext<NotificationHub>()));

        private IHubContext _context;

        private Notification(IHubContext context)
        {
            _context = context;
        }

        public static Notification Instance
        {
            get { return _instance.Value; }
        }

        public static ConnectionMapping<string> ConnectionMapping
        {
            get { return _connections; }
        }

        public void UpdateStatistics()
        {
            StatisticsModel statistics = new StatisticsModel();
            _context.Clients.All.updateStatistics(statistics);
        }

        public void SendTwitterSignUp_SuccessNotification()
        { 
            string connnectionId = HttpContext.Current.Session[Constants.Instance.KEY_Twitter_Notication_ConnectionId].ToString();
            _context.Clients.Client(connnectionId).twitterSignUp_SucccessNotification();
            HttpContext.Current.Session[Constants.Instance.KEY_Twitter_Notication_ConnectionId] = null;
        }

        public void SendTwitterSignUp_ErrorNotification()
        {
            string connnectionId = HttpContext.Current.Session[Constants.Instance.KEY_Twitter_Notication_ConnectionId].ToString();
            _context.Clients.Client(connnectionId).twitterSignUp_ErrorNotification();
            HttpContext.Current.Session[Constants.Instance.KEY_Twitter_Notication_ConnectionId] = null;
        }

        public void SendTwitterAuthorizeCallbackFailureNofication(string userId)
        {
            _context.Clients.Group(userId).twitterAuthorizeCallback_FailureNofication();
        }

        public void SendTwitterAuthorizeCallbackSuccessNofication(string userId)
        {
            _context.Clients.Group(userId).twitterAuthorizeCallback_SuccessNofication();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace luxbsa.Hubs
{
    public class Connection
    {
        public string ConnectionId { get; set; }
        public bool IsConnected { get; set; }
    }

    public class UserConnection
    {
        public string UserIdentifier { get; set; }
        public HashSet<Connection> ConnectionIds { get; set; }
    }

    public class ConnectionMapping<T>
    {
        private readonly Dictionary<T, HashSet<string>> _connections = new Dictionary<T, HashSet<string>>();
        //private readonly HashSet<UserConnection> _connections = new HashSet<UserConnection>();
        private readonly Dictionary<string, UserConnection> _groups = new Dictionary<string, UserConnection>();

        public int Count
        {
            get
            {
                return _connections.Count;
            }
        }

        public void Add(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    connections = new HashSet<string>();
                    _connections.Add(key, connections);
                }

                lock (connections)
                {
                    connections.Add(connectionId);
                }
            }
        }

        public HashSet<string> GetConnections(T key)
        {
            HashSet<string> connections;
            if (_connections.TryGetValue(key, out connections))
            {
                return connections;
            }

            return new HashSet<string>();
            //return Enumerable.Empty<string>();
        }

        public void Remove(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    return;
                }

                lock (connections)
                {
                    connections.Remove(connectionId);

                    if (connections.Count == 0)
                    {
                        _connections.Remove(key);
                    }
                }
            }
        }
    }
}
﻿using luxbsa.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace luxbsa.Models
{
    public class LoginModel
    {
        public LoginModel() 
        {
            Statistics = new StatisticsModel();
        }

        [Required]
        public String Email { get; set; }

        [Required]
        public String Password { get; set; }

        public bool? SignUpCompleted { get; set; }

        public bool? ShowActivationCompleted { get; set; }

        public bool? ShowDeactivationCompleted { get; set; }

        public bool? ShowDisassociationCompleted { get; set; }

        public bool ShowTerms { get; set; }

        private bool? isBeta;
        public bool IsBeta { 
            get 
            {
                if (isBeta == null)
                    return Constants.Instance.Beta;
                else
                    return (bool)isBeta;
            }
            set { isBeta = value; }
        }

        public StatisticsModel Statistics { get; set; }
    }
}

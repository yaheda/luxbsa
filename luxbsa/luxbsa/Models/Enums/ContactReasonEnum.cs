﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace luxbsa.Models.Enums
{
    public enum ContactReasonEnum
    {
        GeneralEnquiry = 0,
        Complaint = 1,
        TechnicalFault = 2,
        Suggestion = 3,
        Other = 4
    }
}
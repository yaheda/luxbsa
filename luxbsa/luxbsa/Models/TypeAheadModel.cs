﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace luxbsa.Models
{
    public class TypeAheadModel
    {
        public TypeAheadModel() { }
        public string key { get; set; }
        public string value { get; set; }
        public string delete { get; set; }
    }
}
﻿using luxbsa.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace luxbsa.Models
{
    public class BusinessModel
    {
        #region Private Vairables

        private Business business = new Business();
        List<UserModel> users = new List<UserModel>();
        List<TypeAheadModel> members = new List<TypeAheadModel>();
        private string sectorName = string.Empty;

        #endregion

        #region Ctor

        public BusinessModel() { }
        public BusinessModel(Business business)
        {
            this.business = business;
            this.sectorName = business.Sector.Name;
        }

        #endregion

        #region Properties

        public Business Entity
        {
            get { return this.business; }
            set { this.business = value; }
        }

        public string SectorName
        {
            get { return this.sectorName; }
            set { this.sectorName = value; }
        }

        public int Id
        {
            get { return this.business.Id; }
            set { this.business.Id = value; }
        }

        [Required]
        public String Name 
        {
            get { return this.business.Name; }
            set { this.business.Name = value; }
        }

        [Required]
        public String RegistrationNumber 
        {
            get { return this.business.RegistrationNumber; }
            set { this.business.RegistrationNumber = value; }
        }

        public String WebsiteLink 
        {
            get { return this.business.WebsiteLink; }
            set { this.business.WebsiteLink = value; }
        }

        [Required]
        public String Description 
        {
            get { return this.business.Description; }
            set { this.business.Description = value; }
        }

        public String Address 
        {
            get { return this.business.Address; }
            set { this.business.Address = value; }
        }

        public String VideoLink 
        {
            get { return this.business.VideoLink; }
            set { this.business.VideoLink = value; }
        }

        public String Country
        {
            get { return this.business.Country; }
            set { this.business.Country = value; }
        }

        public int SectorId 
        {
            get { return this.business.SectorId; }
            set { this.business.SectorId = value; }
        }

        public Dictionary<int, String> Sectors
        {
            get
            {
                List<Sector> sectorList = SectorDAL.GetAll();

                if (sectorList == null)
                    return new Dictionary<int, string>();

                return sectorList.ToDictionary(k => k.Id, v => v.Name);
            }
        }

        public JsonResult Sectors_XEditable
        {
            get
            {
                var x = Sectors;
                //List<Sector> sectorList = SectorDAL.GetAll();
                //var items = new SelectList(sectorList.ToDictionary(k => k.Id, v => v.Name), "value", "text");
                return new JsonResult()
                {
                    Data = Sectors,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }

        public List<TypeAheadModel> Members
        {
            get { return members; }
            set { members = value; }
        }

        public List<UserModel> Users 
        { 
            get 
            {
                this.users = new List<UserModel>();

                if (this.business.Users == null)
                    return this.users;

                foreach (User user in this.business.Users)
                {
                    UserModel userModel = new UserModel(user);
                    this.users.Add(userModel);
                }

                return this.users;
            } 
            set 
            {
                this.users = value;
                List<User> userList = new List<User>();
                foreach (UserModel userModel in this.users)
                {
                    userList.Add(userModel.Entity);
                }
                this.business.Users = userList;
            }
        }

        #endregion
    }
}
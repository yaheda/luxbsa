﻿using luxbsa.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace luxbsa.Models
{
    public class DiaryModel
    {
        private Diary diary = new Diary();
        List<TypeAheadModel> businesses = new List<TypeAheadModel>();
        List<string> tags = new List<string>();

        public DiaryModel(Diary diary)
        {
            this.diary = diary;

            foreach (Business business in this.diary.Businesses)
            {
                TypeAheadModel typeAhead = new TypeAheadModel()
                {
                    key = business.Id.ToString(),
                    value = FormatHelper.GetTypeAheadBusiness(business)
                };
                businesses.Add(typeAhead);
            }
            foreach (Tag tag in this.diary.Tags)
            {
                tags.Add(tag.Text);
            }
        }

        public int Id
        {
            get { return this.diary.Id; }
            set { this.diary.Id = value; }
        }

        public string Title
        {
            get { return this.diary.Title; }
            set { this.diary.Title = value; }
        }

        public string Content
        {
            get { return this.diary.Content; }
            set { this.diary.Content = value; }
        }

        public DateTime DateCreated
        {
            get { return this.diary.DateCreated; }
            set { this.diary.DateCreated = value; }
        }

        public int UserId
        {
            get { return this.diary.UserID; }
            set { this.diary.UserID = value; }
        }

        public List<TypeAheadModel> Businesses
        {
            get { return businesses; }
            set { businesses = value; }
        }

        public List<string> Tags
        {
            get { return tags; }
            set { tags = value; }
        } 
    }
}
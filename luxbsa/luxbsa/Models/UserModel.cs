﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace luxbsa.Models
{
    public class UserModel
    {
        #region Enums

        public enum GenderType
        {
            [Display(Name="None")]
            None = 0,
            [Display(Name="Male")]
            Male = 1,
            [Display(Name = "Female")]
            Female = 2
        }

        public enum ActivationStatus
        { 
            InActive = 0,
            Active = 1
        }

        #endregion

        #region Private variables

        User user = new User();
        List<BusinessModel> businesses = new List<BusinessModel>();

        #endregion

        #region Ctor

        public UserModel() { }
        public UserModel(User user) 
        {
            this.user = user;
        }

        #endregion

        #region Properties

        public User Entity
        {
            get { return this.user; }
            set { this.user = value; }
        }

        public int Id 
        { 
            get { return this.user.Id; } 
            set { this.user.Id = value; } 
        }

        [Required]
        public String Firstname 
        { 
            get { return this.user.Firstname; } 
            set { this.user.Firstname = value; } 
        }

        [Required]
        public String Surname 
        {
            get { return this.user.Surname; }
            set { this.user.Surname = value; }
        }

        [Required]
        public String Password 
        {
            get { return this.user.Password; }
            set { this.user.Password = value; }
        }

        public GenderType Gender 
        {
            get 
            {
                if (this.user.Gender == null)
                    return GenderType.None;
                return (GenderType)this.user.Gender; 
            }
            set 
            {
                if (value == null)
                    this.user.Gender = 0;
                else
                    this.user.Gender = Convert.ToInt32(value);
            }
        }

        [Required]
        [EmailAddress]
        public String Email 
        {
            get { return this.user.Email; }
            set { this.user.Email = value; }
        }

        public DateTime? DateOfBirth 
        {
            get { return this.user.DateOfBirth; }
            set { this.user.DateOfBirth = value; }
        }

        public String AboutMe 
        {
            get { return this.user.AboutMe; }
            set { this.user.AboutMe = value; }
        }

        public bool deleted { get; set; }

        public List<BusinessModel> Businesses
        {
            get
            {
                this.businesses = new List<BusinessModel>();
                
                if (this.user.Businesses == null)
                    return this.businesses;

                foreach (Business business in this.user.Businesses)
                {
                    BusinessModel businessModel = new BusinessModel(business);
                    this.businesses.Add(businessModel);
                }

                return this.businesses;
            }
            set
            {
                this.businesses = value;
                List<Business> businessList = new List<Business>();
                foreach (BusinessModel businessModel in this.businesses)
                {
                    businessList.Add(businessModel.Entity);
                }
                this.user.Businesses = businessList;
            }
        }
        #endregion
    }
}
﻿using luxbsa.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace luxbsa.Models
{
    public class StatisticsModel
    {
        public StatisticsModel()
        {
            RefreshStats();
        }

        public int NUsers { get; set; }
        public int NBusinesses { get; set; }
        public int NDiaryEntries { get; set; }

        public void RefreshStats()
        { 
            using(luxbsaEntities db = new luxbsaEntities())
            {
                NUsers = UserDAL.GetTotal(db);
                NBusinesses = 0; //BusinessDAL.GetTotal(db);
                NDiaryEntries = 0; //DiaryDAL.GetTotal(db);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace luxbsa.Models
{
    public class SectorModel
    {
        private Sector sector = new Sector();

        public SectorModel() { }
        public SectorModel(Sector sector)
        {
            this.sector = sector;
        }
        
        public int Id
        {
            get { return this.sector.Id; }
            set { this.sector.Id = value; }
        }

        public string Name
        {
            get { return this.sector.Name; }
            set { this.sector.Name = value; }
        }
    }
}
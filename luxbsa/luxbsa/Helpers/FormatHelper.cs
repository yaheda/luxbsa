﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace luxbsa.Helpers
{
    public class FormatHelper
    {
        public static string GetTypeAheadBusiness(Business business)
        {
            return business.Name + " (" + business.Country + ")";
        }

        public static dynamic GetDynamicDate(DateTime dateTime)
        { 
            var Date = new 
            {
                Day = dateTime.Day, 
                IsDaySingleDigit = (dateTime.Day.ToString().Length == 1),
                Month = dateTime.Month,
                IsMonthSingleDigit = (dateTime.Month.ToString().Length == 1),
                Year = dateTime.Year
            };

            return Date;
        }
    }
}
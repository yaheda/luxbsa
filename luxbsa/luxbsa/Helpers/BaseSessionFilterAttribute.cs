﻿using luxbsa.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace luxbsa.Helpers
{
    public class BaseSessionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var sessionAlive = false;
            var sessionKeys = filterContext.HttpContext.Session.Keys;

            foreach (string key in sessionKeys)
            { 
                if (key == Constants.Instance.KEY_User_Id) 
                { 
                    sessionAlive = true; 
                    break; 
                }
            }


            if (!sessionAlive)
            {
                using (luxbsaEntities db = new luxbsaEntities())
                {
                    int userId = Convert.ToInt32(filterContext.HttpContext.User.Identity.Name);
                    User user = UserDAL.GetById(db, userId);

                    filterContext.HttpContext.Session[Constants.Instance.KEY_User_Id] = userId;
                    filterContext.HttpContext.Session[Constants.Instance.KEY_User_Firstname] = user.Firstname;
                    filterContext.HttpContext.Session[Constants.Instance.KEY_User_Surname] = user.Surname;
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace luxbsa.Helpers
{
    public class Constants
    {
        private static Constants s_instance;
        public static Constants Instance
        {
            get
            {
                if (s_instance == null)
                    s_instance = new Constants();
                return s_instance;
            }            
        }

        public bool Debug { get { return Convert.ToBoolean(ConfigurationManager.AppSettings["debug"]); } }
        public bool Beta { get { return Convert.ToBoolean(ConfigurationManager.AppSettings["beta"]); } }
        public string Domain { get { return ConfigurationManager.AppSettings["domain"]; } }
        public string KEY_Domain { get { return "domain"; } } //TODO: remember email helper image

        public string LuxbsaActivate_Link { get { return "http://" + Domain + "/Account/Activate"; } }
        public string LuxbsaDeactivate_Link { get { return "http://" + Domain + "/Account/Deactivate"; } }
        public string LuxbsaDisassociate_Link { get { return "http://" + Domain + "/Account/Disassociate"; } }
        public string LuxbsaTerms_Link { get { return "http://" + Domain + "/Account/Terms"; } }

        public string KEY_SecurityKey { get { return "securityKey"; } }
        public string KEY_SecurityIV { get { return "securityIV"; } }

        public string KEY_User_Id { get { return "User.Id"; } }
        public string KEY_User_Firstname { get { return "User.Firstname"; } }
        public string KEY_User_Surname { get { return "User.Surname"; } }
        public string KEY_User_Email { get { return "User.Email"; } }

        public string KEY_Twitter_ConsumerKey { get { return "twitter_consumerKey"; } }
        public string KEY_Twitter_ConsumerSecret { get { return "twitter_consumerSecret"; } }
        public string KEY_Twitter_AccessToken { get { return "twitter_accessToken"; } }
        public string KEY_Twitter_AccessTokenSecret { get { return "twitter_accessTokenSecret"; } }
        public string KEY_Twitter_Notication_ConnectionId { get { return "twiiter_notification_ConnectionId"; } }

        public string KEY_LuxbsaEmailAddress_Info { get { return "luxbsa.emailAddress"; } }
        public string KEY_LuxbsaEmailPassword { get { return "luxbsa.emailpassword"; } }

        public string KEY_SendgridUsername { get { return "sendgrid.username"; } }
        public string KEY_SendgridPassword { get { return "sendgrid.password"; } }

        public string KEY_LoginModel { get { return "LoginModel"; } }

        public MailAddress LUXBSA_MailAddress { get { return new MailAddress(ConfigurationManager.AppSettings[Constants.Instance.KEY_LuxbsaEmailAddress_Info], "LUX BSA"); } }

        



        //public const bool debug = Convert.ToBoolean( ConfigurationManager.AppSettings["debug"]);
        //public const string Domain = debug == true ? "localhost:2955" : "luxbsa.com";

        //public const string SecurityKey = "securityKey";
        //public const string SecurityIV = "securityIV";

        //public const string User_Id = "User.Id";
        //public const string User_Firstname = "User.Firstname";
        //public const string User_Surname = "User.Surname";

        //public const string User_Email = "User.Email";

        //public const string Twitter_ConsumerKey = "twitter_consumerKey";
        //public const string Twitter_ConsumerSecret = "twitter_consumerSecret";
        //public const string Twitter_AccessToken = "twitter_accessToken";
        //public const string Twitter_AccessTokenSecret = "twitter_accessTokenSecret";
        //public const string Twitter_Notication_ConnectionId = "twiiter_notification_ConnectionId";

        //public const string LuxbsaEmailAddress = "luxbsa.emailAddress";
        //public const string LuxbsaEmailPassword = "luxbsa.emailpassword";        

        //public const string LuxbsaActivate_Link = "http://" + Domain +"/Account/Activate";
        //public const string LuxbsaDeactivate_Link = "http://" + Domain + "/Account/Deactivate";
        //public const string LuxbsaDisassociate_Link = "http://" + Domain + "/Account/Disassociate";
        //public const string LuxbsaTerms_Link = "http://" + Domain + "/Account/Terms";

        //public const string SendgridUsername = "sendgrid.username";
        //public const string SendgridPassword = "sendgrid.password";
    }
}
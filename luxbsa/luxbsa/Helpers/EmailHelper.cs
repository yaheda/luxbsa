﻿using SendGridMail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;

namespace luxbsa.Helpers
{
    public static class EmailHelper
    {
        public static void SendEmail(MailAddress from, MailAddress to, string subject, string body)
        {
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.sendgrid.net";//"relay-hosting.secureserver.net";//"smtp.luxbsa.com";//"smtpout.secureserver.net";
            smtp.Port = 587;
            smtp.Timeout = 10000;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;

            var username = ConfigurationManager.AppSettings[Constants.Instance.KEY_SendgridUsername];
            var password = ConfigurationManager.AppSettings[Constants.Instance.KEY_SendgridPassword];
            smtp.Credentials = new System.Net.NetworkCredential(username, password);

            MailMessage mailMsg = new MailMessage();

            mailMsg.To.Add(to);
            mailMsg.From = from;

            mailMsg.Subject = subject;

            mailMsg.Body = body;

            //mailMsg.Body = body.Replace(Environment.NewLine, "<BR />");
            mailMsg.Body = Regex.Replace(mailMsg.Body, @"\r\n?|\n", "<br />");

            mailMsg.IsBodyHtml = true;

            mailMsg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            smtp.Send(mailMsg);
        }

        public static Email CreateRegistrationEmail(string name, string token)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            var subject = "LUX-BSA Registration Verification";
            var body =
                "<div style='padding: 40px'>" +
                "<div><img src='http://" + Constants.Instance.Domain + "/Content/img/logo/logo.png' width='100' /></div>" +
                "<br />" +
                "<div>Hi "+ textInfo.ToTitleCase(name) +"</div>" +
                "<br />" +
                "<div>Thank you for using signing up with LUX-BSA!</div>" +
                "<div>To complete the registration process, please click on the link below.</div>" +
                "<br />" +
                "<div><a href='" + Constants.Instance.LuxbsaActivate_Link + "?token=" + token + "'>" + Constants.Instance.LuxbsaActivate_Link + "?token=" + token + "</a></div>" +
                "<br />" +
                "<div>Kind Regards from the LUX-BSA team</div>" +
                "<br />" +
                "<div style='font-size: 0.7em'>" +
                    "<div>If you did not sign up with LUX-BSA please <a href='" + Constants.Instance.LuxbsaDisassociate_Link + "?token=" + token + "'>click here</a> to disassociate yourself from us.</div>" +
                    "<div>Please note that this email is subject to the following <a href='" + Constants.Instance.LuxbsaTerms_Link + "'>terms & conditions.</a></div>" +
                "</div>" +
                "</div>";

            return new Email
            {
                Subject = subject,
                Body = body
            };
        }
    }

    public class Email
    {
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}